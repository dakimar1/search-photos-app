import React from 'react'
import {
  Pagination, PaginationItem, PaginationLink
} from 'reactstrap'

class PaginationDeck extends React.Component {
  constructor (props) {
    super(props)
    this.doGetMoreResults = this.doGetMoreResults.bind(this)
    this.state = {
      // eslint-disable-next-line react/prop-types
      data: this.props.data,
      page: 1
    }
  }

  doGetMoreResults (event) {
    this.setState({ page: parseInt(event.target.value) })
    // eslint-disable-next-line react/prop-types
    this.props.parentToggle(event.target.value)
  }

  render () {
    // eslint-disable-next-line react/prop-types
    const data = this.props.data
    const page = this.state.page
    // eslint-disable-next-line react/prop-types
    const nbrPagination = data.total_pages

    const pagination = []

    for (var i = 1; i <= nbrPagination; i++) {
      pagination.push(
        <PaginationItem key={i} active={i === this.state.page}> <PaginationLink onClick={this.doGetMoreResults} value={i}>
          {i}
        </PaginationLink> </PaginationItem>
      )
    }

    const previousPage = page - 1
    const nextPage = page + 1

    return (

      <Pagination className="pagination-style" aria-label="Page navigation example"> <PaginationItem disabled={page === 1}>
        <PaginationLink first value="1" onClick={this.doGetMoreResults}/> </PaginationItem>
      <PaginationItem disabled={page === 1}>
        <PaginationLink previous value={previousPage} onClick={this.doGetMoreResults}/> </PaginationItem>

      {pagination}

      <PaginationItem disabled={page === nbrPagination}>
        <PaginationLink next value={nextPage} onClick={this.doGetMoreResults}/> </PaginationItem>
      <PaginationItem disabled={page === nbrPagination}>
        <PaginationLink last value={nbrPagination} onClick={this.doGetMoreResults}/> </PaginationItem>

      </Pagination>

    )
  }
}

export default PaginationDeck
