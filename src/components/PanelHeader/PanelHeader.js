
import React from 'react'

class PanelHeader extends React.Component {
  render () {
    return (
      <div
        className={
          'panel-header ' +
          // eslint-disable-next-line react/prop-types
          (this.props.size !== undefined
            // eslint-disable-next-line react/prop-types
            ? 'panel-header-' + this.props.size
            : '')
        }
      >
        {/* eslint-disable-next-line react/prop-types */}
        {this.props.content}
      </div>
    )
  }
}

export default PanelHeader
