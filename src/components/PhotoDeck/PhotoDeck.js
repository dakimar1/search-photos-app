import React from 'react'
import {
  Card, Button, CardImg, CardTitle, CardDeck,
  CardSubtitle, CardBody, CardFooter
} from 'reactstrap'

class PhotoDeck extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      // eslint-disable-next-line react/prop-types
      data: this.props.data,
      page: 1
    }
  }

  render () {
    // eslint-disable-next-line react/prop-types
    const data = this.props.data
    // eslint-disable-next-line react/prop-types
    const listPhotoDeck = data.results.map((photo, index) => {
      return (
        <Card className="card-style" key={index}>
          <CardImg top src={photo.urls.small} alt="Card image cap" />
          <CardBody className="card-body-style">
            { photo.alt_description == null ? <CardTitle>{photo.description}</CardTitle> : <CardTitle>{photo.alt_description}</CardTitle>}
            <CardSubtitle>
              {

                photo.tags.map((tag, index) => {
                  return (<a href="#" key={index} className="alert-link">#{tag.title}</a>)
                })
              }
            </CardSubtitle>

          </CardBody>
          <CardFooter >
            <Button href={photo.links.download} target="_blank">
              <i className="fas fa-file-download fa-2x"/>
            </Button>
          </CardFooter>
        </Card>
      )
    })

    return (

      <CardDeck >
        {listPhotoDeck}
      </CardDeck>

    )
  }
}

export default PhotoDeck
