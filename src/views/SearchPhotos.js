import React from 'react'

// reactstrap components
import {
  Spinner,
  Alert,
  Row,
  Col,
  InputGroup, Input, InputGroupAddon, InputGroupText
} from 'reactstrap'

// core components
import PanelHeader from 'components/PanelHeader/PanelHeader.js'
import PhotoDeck from 'components/PhotoDeck/PhotoDeck.js'
import PaginationDeck from 'components/PaginationDeck/PaginationDeck.js'
import { getSearchPhotos, getMoreResults } from 'api/ApiSearchPhotos.js'

class SearchPhotos extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      display: false,
      counter: 0,
      tag: '',
      tagSearch: '',
      data: {}
    }
    this.doParentGetMoreResults = this.doParentGetMoreResults.bind(this)
    this.doSearchPhotos = this.doSearchPhotos.bind(this)
    this.changeTag = this.changeTag.bind(this)
  }

  doParentGetMoreResults (page) {
    this.setState({ loading: true })
    getMoreResults(this.state.tagSearch, page).then(json => {
      this.setState({ loading: false, data: json })
    })
  }

  doSearchPhotos (event) {
    this.setState({ loading: true, display: true, tagSearch: this.state.tag })
    getSearchPhotos(this.state.tag).then(json => {
      this.setState({ loading: false, data: json })
    })
    event.preventDefault()
  }

  changeTag (event) {
    this.setState({ tag: event.target.value })
  }

  render () {
    const loading = this.state.loading
    const display = this.state.display
    const data = this.state.data

    return (
      <>
        <PanelHeader
          content={
            <div className="header text-center">
              <h2 className="title">Search photos</h2>
              <div className="category">
                <form onSubmit={this.doSearchPhotos}>
                  <InputGroup className="no-border text-white input-search">
                    <Input className=" text-white" onChange={this.changeTag} placeholder="Search..."/>
                    <InputGroupAddon addonType="append"> <InputGroupText>
                      <i onClick={this.doSearchPhotos} className="now-ui-icons ui-1_zoom-bold text-white"/> </InputGroupText> </InputGroupAddon>
                  </InputGroup>
                </form>

              </div>
            </div>
          }
        />

        <div className="content">

          {display
            ? <div>
                <Row className="justify-content-md-center card-deck-style">
                  {loading
                    ? <Col xs="auto"> <Spinner color="dark" className="spinner-style" style={{ width: '3rem', height: '3rem' }}/> </Col>
                    : <div>
                        {data.total > 0
                          ? <Col xs="auto"> <PhotoDeck data={data}/> </Col>
                          : <Col xs="12">
                              <Alert color="dark">Your search - {this.state.tagSearch} - did not match any Photos.

                        Suggestions:
                              <ul>
                                <li>Make sure that all words are spelled correctly.</li>
                                <li>Try different keywords.</li>
                                <li>Try more general keywords.</li>
                              </ul>
                              </Alert>
                            </Col>
                        }
                      </div>
                  }
                </Row> <Row className="justify-content-md-center"> <Col xs="auto">
                  <PaginationDeck className="pagination-style" data={data} parentToggle={this.doParentGetMoreResults}/>
                </Col> </Row>
              </div>
            : <Alert className="alert-style" color="light">
              Find the best photos
              </Alert>

          }
        </div>
      </>
    )
  }
}

export default SearchPhotos
