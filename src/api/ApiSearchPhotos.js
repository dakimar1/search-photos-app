import { unsplash } from './http-constants'
import { toJson } from 'unsplash-js'

const MAX_RESULTS = 10

export const getSearchPhotos = (tag) => {
  return unsplash.search.photos(tag, 1, MAX_RESULTS, { orientation: 'portrait', color: 'green' })
    .then(toJson)
}

export const getMoreResults = (tag, i) => {
  return unsplash.search.photos(tag, i, MAX_RESULTS, { orientation: 'portrait', color: 'green' })
    .then(toJson)
}
