import Unsplash from 'unsplash-js'
const TIME_OUT = process.env.VUE_APP_TIMEOUT
const APP_ACCESS_KEY = 'fOWo4tTnlcjVnTKSfh6RXxehsueLnAdVtFEq3SSNH08'

export const unsplash = new Unsplash({
  accessKey: APP_ACCESS_KEY,
  // Optionally you can also configure a custom header to be sent with every request
  headers: {
    'X-Custom-Header': 'foo'
  },
  // Optionally if using a node-fetch polyfill or a version of fetch which supports the timeout option, you can configure the request timeout for all requests
  timeout: TIME_OUT // values set in ms
})
