import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Router, Route, Switch, Redirect } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css'
import 'assets/scss/now-ui-dashboard.scss?v1.4.0'
import 'assets/css/Style.css'

import Layout from 'layouts/layout.js'

const hist = createBrowserHistory()

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/layout" render={(props) => <Layout {...props} />} />
      <Redirect from="*" to="/layout/search-photos" />
      <Redirect from="/" to="/layout/search-photos" />
    </Switch>
  </Router>,
  document.getElementById('root')
)
