
import React from 'react'

import PerfectScrollbar from 'perfect-scrollbar'

import { Route, Switch, Redirect } from 'react-router-dom'

import Header from 'components/Navbars/Header.js'
import Footer from 'components/Footer/Footer.js'
import routes from 'routes.js'

var ps

class layout extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      backgroundColor: 'blue',
      // eslint-disable-next-line react/prop-types
      data: this.props.data,
      page: 1
    }
    this.mainPanel = React.createRef()
  }

  componentDidMount () {
    if (navigator.platform.indexOf('Win') > -1) {
      ps = new PerfectScrollbar(this.mainPanel.current)
      document.body.classList.toggle('perfect-scrollbar-on')
    }
  }

  componentWillUnmount () {
    if (navigator.platform.indexOf('Win') > -1) {
      ps.destroy()
      document.body.classList.toggle('perfect-scrollbar-on')
    }
  }

  componentDidUpdate (e) {
    if (e.history.action === 'PUSH') {
      document.documentElement.scrollTop = 0
      document.scrollingElement.scrollTop = 0
      this.mainPanel.current.scrollTop = 0
    }
  }

  render () {
    return (
      <div className="wrapper">
        <div className="main-panel" ref={this.mainPanel}>
          <Header {...this.props} />
          <Switch>
            {routes.map((prop, key) => {
              return (
                <Route
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />
              )
            })}
            <Redirect from="*" to="/layout/search-photos" />
          </Switch>
          <Footer fluid />
        </div>
      </div>
    )
  }
}

export default layout
