
import SearchPhotos from 'views/SearchPhotos.js'

var dashRoutes = [
  {
    path: '/search-photos',
    name: 'Search Photos',
    icon: 'design_image',
    component: SearchPhotos,
    layout: '/layout'
  }
]

export default dashRoutes
